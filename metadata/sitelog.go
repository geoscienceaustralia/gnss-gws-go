package metadata

import (
	"net/url"
	"time"
)

type SiteLog struct {
	ID                 int                `json:"id"`
	LastModifiedDate   time.Time          `json:"lastModifiedDate"`
	SiteIdentification SiteIdentification `json:"siteIdentification"`
	SiteLocation       SiteLocation       `json:"siteLocation"`
	GnssReceivers      []GnssReceivers    `json:"gnssReceivers"`
	GnssAntennas       []GnssAntennas     `json:"gnssAntennas"`
}

type SiteIdentification struct {
	SiteName            string    `json:"siteName"`
	FourCharacterID     string    `json:"fourCharacterId"`
	IersDOMESNumber     string    `json:"iersDOMESNumber"`
	MonumentDescription string    `json:"monumentDescription"`
	HeightOfMonument    string    `json:"heightOfMonument"`
	DateInstalled       time.Time `json:"dateInstalled"`
}

type SiteLocation struct {
	City                string `json:"city"`
	State               string `json:"state"`
	Country             string `json:"country"`
	TectonicPlate       string `json:"tectonicPlate"`
	ApproximatePosition struct {
		CartesianPosition struct {
			Type        string    `json:"type"`
			Coordinates []float64 `json:"coordinates"`
		} `json:"cartesianPosition"`
		GeodeticPosition struct {
			Type        string    `json:"type"`
			Coordinates []float64 `json:"coordinates"`
		} `json:"geodeticPosition"`
	} `json:"approximatePosition"`
	Notes string `json:"notes"`
}

type EffectiveDates struct {
	From time.Time `json:"from"`
	To   time.Time `json:"to"`
}

type GnssReceivers struct {
	DateInserted             time.Time      `json:"dateInserted"`
	DateDeleted              time.Time      `json:"dateDeleted"`
	DeletedReason            string         `json:"deletedReason"`
	Type                     string         `json:"type"`
	SatelliteSystem          string         `json:"satelliteSystem"`
	SerialNumber             string         `json:"serialNumber"`
	FirmwareVersion          string         `json:"firmwareVersion"`
	ElevationCutoffSetting   string         `json:"elevationCutoffSetting"`
	DateInstalled            time.Time      `json:"dateInstalled"`
	DateRemoved              time.Time      `json:"dateRemoved"`
	TemperatureStabilization interface{}    `json:"temperatureStabilization"`
	Notes                    string         `json:"notes"`
	EffectiveDates           EffectiveDates `json:"effectiveDates"`
}

type GnssAntennas struct {
	DateInserted           time.Time      `json:"dateInserted"`
	DateDeleted            time.Time      `json:"dateDeleted"`
	DeletedReason          string         `json:"deletedReason"`
	Type                   string         `json:"type"`
	SerialNumber           string         `json:"serialNumber"`
	AntennaReferencePoint  string         `json:"antennaReferencePoint"`
	MarkerArpUpEcc         float64        `json:"markerArpUpEcc"`
	MarkerArpNorthEcc      float64        `json:"markerArpNorthEcc"`
	MarkerArpEastEcc       float64        `json:"markerArpEastEcc"`
	AlignmentFromTrueNorth string         `json:"alignmentFromTrueNorth"`
	AntennaRadomeType      string         `json:"antennaRadomeType"`
	RadomeSerialNumber     string         `json:"radomeSerialNumber"`
	AntennaCableType       string         `json:"antennaCableType"`
	AntennaCableLength     string         `json:"antennaCableLength"`
	DateInstalled          time.Time      `json:"dateInstalled"`
	DateRemoved            time.Time      `json:"dateRemoved"`
	Notes                  string         `json:"notes"`
	EffectiveDates         EffectiveDates `json:"effectiveDates"`
}

func GetSitelogByFourCharacterID(fourCharacterId string) (SiteLog, error) {
	params := url.Values{}
	params.Add("id", fourCharacterId)

	u := url.URL{
		Scheme:   "https",
		Host:     "gws.geodesy.ga.gov.au",
		Path:     "siteLogs/search/findByFourCharacterId",
		RawQuery: params.Encode(),
	}

	var sitelog SiteLog
	if err := getAndDecode(u.String(), &sitelog); err != nil {
		return SiteLog{}, err
	}

	return sitelog, nil
}

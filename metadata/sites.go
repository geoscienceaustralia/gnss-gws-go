package metadata

import (
	"fmt"
)

type NetworkTenancy struct {
	ID int `json:"corsNetworkId"`
	// Period
}

type ApproximatePosition struct {
	Coordinates []float64 `json:"coordinates"`
}

type CORSSite struct {
	ID                  int                 `json:"id"`
	FourCharacterID     string              `json:"fourCharacterId"`
	Name                string              `json:"name"`
	Description         string              `json:"description"`
	ApproximatePosition ApproximatePosition `json:"approximatePosition"`
	SiteStatus          string              `json:"siteStatus"`
	NetworkTenancies    []NetworkTenancy    `json:"networkTenancies"`
}

type CORSSitesResponse struct {
	Embedded struct {
		CORSSites []CORSSite `json:"corsSites"`
	} `json:"_embedded"`
	Links Links `json:"_links"`
	Page  Page  `json:"page"`
}

func GetCORSSites() ([]CORSSite, error) {
	var sitesResp CORSSitesResponse
	if err := getAndDecode("https://gws.geodesy.ga.gov.au/corsSites?size=700", &sitesResp); err != nil {
		return []CORSSite{}, err
	}

	sites := sitesResp.Embedded.CORSSites
	for sitesResp.Links.Next.HREF != "" {
		var nextResp CORSSitesResponse
		if err := getAndDecode(sitesResp.Links.Next.HREF, &nextResp); err != nil {
			return sites, err
		}
		sites = append(sites, nextResp.Embedded.CORSSites...)
		sitesResp = nextResp
	}

	return sites, nil
}

func GetCORSSiteByFourCharacterID(fourCharacterID string) (site CORSSite, err error) {
	err = getAndDecode("https://gws.geodesy.ga.gov.au/corsSites/search/findByFourCharacterId?id="+fourCharacterID, &site)
	return site, err
}

func GetCORSSitesByNetworkName(networkName string) (sites []CORSSite, err error) {
	network, err := GetCORSNetworkByName(networkName)
	if err != nil {
		return sites, fmt.Errorf("error getting network: %s", err)
	}

	sitesResp, err := GetCORSSites()
	if err != nil {
		return sites, fmt.Errorf("error getting CORS sites: %s", err)
	}

	for _, site := range sitesResp {
		for _, tenancy := range site.NetworkTenancies {
			if tenancy.ID == network.ID {
				sites = append(sites, site)
			}
		}
	}

	return sites, nil
}

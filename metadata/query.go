package metadata

import (
	"encoding/json"
	"net/http"
)

type Link struct {
	HREF string `json:"href"`
}

type Links struct {
	Next  Link
	First Link
	Last  Link
	// etc.
}

type Page struct {
	Size          int `json:"size"`
	TotalElements int `json:"totalElements"`
	TotalPages    int `json:"totalPages"`
	Number        int `json:"number"`
}

func getAndDecode(url string, target interface{}) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return json.NewDecoder(resp.Body).Decode(target)
}

package metadata

import (
	"net/url"
	"strconv"
	"time"
)

type Period struct {
	From time.Time `json:"from"`
	To   time.Time `json:"to"`
}

type EquipmentInUse struct {
	ID struct {
		EquipmentType string `json:"equipmentType"`
		Type          string `json:"type"`
		SerialNumber  string `json:"serialNumber"`
		Manufacturer  string `json:"manufacturer"`
	} `json:"id"`
	Configuration map[string]interface{} `json:"configuration"` // ree
	Period        Period                 `json:"period"`
}

type Setup struct {
	ID              int              `json:"id"`
	SiteID          int              `json:"siteId"`
	Type            string           `json:"type"`
	EffectivePeriod Period           `json:"effectivePeriod"`
	Invalidated     bool             `json:"invalidated"`
	Current         bool             `json:"current"`
	EquipmentInUse  []EquipmentInUse `json:"equipmentInUse"`
	// _links
}

type SetupsResponse struct {
	Embedded struct {
		Setups []Setup `json:"setups"`
	} `json:"_embedded"`
	Links Links `json:"_links"`
	Page  Page  `json:"page"`
}

func GetSetupBySiteID(siteID int, setupType string) ([]Setup, error) {
	params := url.Values{}
	params.Add("siteId", strconv.Itoa(siteID))
	params.Add("type", setupType)
	params.Add("size", "100")

	u := url.URL{
		Scheme:   "https",
		Host:     "gws.geodesy.ga.gov.au",
		Path:     "setups/search/findBySiteId",
		RawQuery: params.Encode(),
	}

	// TODO: Pagination
	var setups SetupsResponse
	if err := getAndDecode(u.String(), &setups); err != nil {
		return []Setup{}, err
	}

	return setups.Embedded.Setups, nil
}

func GetCurrentSetupByFourCharacterID(fourCharacterID string) (setup Setup, err error) {
	err = getAndDecode("https://gws.geodesy.ga.gov.au/setups/search/findCurrentByFourCharacterId?type=CorsSetup&id="+fourCharacterID, &setup)
	return setup, err
}

package metadata

import "fmt"

type CORSNetwork struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	// Links
}

type CORSNetworksResponse struct {
	Embedded struct {
		CORSNetworks []CORSNetwork `json:"corsNetworks"`
	} `json:"_embedded"`
	Links Links `json:"_links"`
	Page  Page  `json:"page"`
}

func GetCORSNetworks() ([]CORSNetwork, error) { // a map of id to name might be more useful
	var networksResp CORSNetworksResponse
	err := getAndDecode("https://gws.geodesy.ga.gov.au/corsNetworks", &networksResp)
	if err != nil {
		return []CORSNetwork{}, err
	}

	networks := networksResp.Embedded.CORSNetworks
	for networksResp.Links.Next.HREF != "" {
		var nextResp CORSNetworksResponse
		if err := getAndDecode(networksResp.Links.Next.HREF, &nextResp); err != nil {
			return networks, err
		}
		networks = append(networks, nextResp.Embedded.CORSNetworks...)
		networksResp = nextResp
	}
	return networks, nil
}

func GetCORSNetworkByName(networkName string) (network CORSNetwork, error error) {
	networks, err := GetCORSNetworks()
	if err != nil {
		return network, fmt.Errorf("error getting networks: %s", err)
	}

	for _, network := range networks {
		if network.Name == networkName {
			return network, nil
		}
	}

	return network, fmt.Errorf("no network found with name %q", networkName)
}
